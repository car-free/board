package board;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;

import java.io.File;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.util.ObjectUtils;

import board.board.dto.FlightDto;
import board.utilities.APIInit;

import java.util.Calendar;



public class JavaTests {
	@Autowired
	private ApplicationContext applicationContext;
	//@Test
	public void EmtpyTest() {
		String a = null;
		if(ObjectUtils.isEmpty(a)==true) { //이렇게 해야 nullpoint Exception 을 막을수 있다.
		//if(a.isEmpty()==true) {// 이렇게 해도 nullpoint Exception을 방어할수 없음 
			System.out.println("NONE!~~~~");
		}else {
			System.out.println("Exist!!!");
			System.out.println(a.indexOf("x"));
		}
	}
	
	//@Test
	public void TimePatternTest() {
		
		/*for (int i = 0; i < 100; i++) {
			String newFileName = Long.toString(System.nanoTime());
			System.out.println(newFileName);
		}*/
		DateTimeFormatter nanoFormat = DateTimeFormatter.ofPattern("yyyyMMddHHmmss_n"); //나노
		DateTimeFormatter miliFormat = DateTimeFormatter.ofPattern("yyyyMMddHHmmss_A"); //밀리
		for (int i = 0; i < 100; i++) {
			ZonedDateTime current = ZonedDateTime.now();
			System.out.println("mili : " + current.format(miliFormat));
			System.out.println("nano : "+current.format(nanoFormat));
		}
	}
	
	//@Test
	public void byteTest() {
		String sValue = "bangno";
		//System.out.println(sValue.getBytes());
		byte[] sByte = sValue.getBytes();
		for (int i = 0; i < sByte.length; i++) {
			System.out.print(sByte[i] + " ");
		}
		//byte [] sByte = sValue.getBy 
	}
	
	//@Test
	public void hashMapTest() {
		HashMap<String, String> sungNam = new HashMap<String, String> ();
		sungNam.put("upperCity","Kyunggi" );
		sungNam.put("mayor", "eun_su_mi");
		sungNam.put("subway","namHansansung" );
		
		ArrayList<String> aList = new ArrayList<String>();
		aList.add("a");
		aList.add("b");
		aList.add("c");
		
		
		HashMap<String, Object> a = new HashMap<String, Object> ();
		a.put("name","park" );
		a.put("age", "122");
		a.put("home",sungNam );
		a.put("etc",aList );
		System.out.println(a.toString());
		
		LinkedHashMap<String, Object> aLinked = new LinkedHashMap<String, Object> ();
		aLinked.put("name","park" );
		aLinked.put("age", "122");
		aLinked.put("home",sungNam );
		
		aLinked.put("etc",aList );
		System.out.println("\nPut한 순서대로 출력 \n"+aLinked.toString());
	}
	
	//@Test
	public void iteratorTest() {
		/*test1*/
		ArrayList<String> aList = new ArrayList<String>();
		Iterator<String> iter = aList.iterator(); 
		aList.add("aaa");
		aList.add("bbb");
		aList.add("ccc");
		
		iter = aList.iterator();
		
		while(iter.hasNext()) {
			System.out.println(iter.next());
		}
		
		/*test2 map을 iterator로 가져오기 */
		System.out.println("\n test2 start~~~");
		HashMap <String,String> hm = new HashMap<String,String>();
		hm.put("park", "parking");
		hm.put("look", "looking");
		hm.put("back", "backing");
		hm.put("ing", "inging");
		
		//바로 반복으로 할순 없고 일단 key값을 set에 담는다.
		Set<String> keys = hm.keySet();
		Iterator<String> keyIter = keys.iterator();
		String keyVal = "";
		while(keyIter.hasNext()) {
			keyVal = keyIter.next();
			System.out.println(hm.get(keyVal));
		}
				
	}
	
	//@Test
	public void arraySettingTest() {
		ArrayList<FlightDto> fruit = new ArrayList<FlightDto>();
		FlightDto flightDto1 = new FlightDto();
		FlightDto flightDto2 = new FlightDto();
		FlightDto flightDto3 = new FlightDto();
		FlightDto flightDto4 = new FlightDto();
		
		flightDto1.setArrivalAirportFsCode("1aa");
		flightDto1.setCarrierFlightNumber("1bb");
		flightDto1.setDepartureAirportName("1cc");
		
		flightDto2.setArrivalAirportFsCode("2aa");
		flightDto2.setCarrierFlightNumber("2bb");
		flightDto2.setDepartureAirportName("2cc");
		
		flightDto3.setArrivalAirportFsCode("3aa");
		flightDto3.setCarrierFlightNumber("3bb");
		flightDto3.setDepartureAirportName("3cc");
		
		flightDto4.setArrivalAirportFsCode("4aa");
		flightDto4.setCarrierFlightNumber("4bb");
		flightDto4.setDepartureAirportName("4cc");
		
		fruit.add(0,flightDto1 );
		fruit.add(1,flightDto2 );
		fruit.add(2,flightDto3 );
		fruit.add(3,flightDto4 );
		
		int index = 0;
		for(FlightDto f : fruit) {
			System.out.println("getArrivalAirportFsCode==> " + f.getArrivalAirportFsCode());
			System.out.println("getCarrierFlightNumber==> " + f.getCarrierFlightNumber());
			System.out.println("getDepartureAirportName==> " + f.getDepartureAirportName()+"\n");
			index ++;
			f.setCarrierName(Integer.toString(index));
		}
		
		for(FlightDto f : fruit) {
			System.out.println("getArrivalAirportFsCode==> " + f.getArrivalAirportFsCode());
			System.out.println("getCarrierFlightNumber==> " + f.getCarrierFlightNumber());
			System.out.println("getDepartureAirportName==> " + f.getDepartureAirportName());
			System.out.println("setCarrierName" + f.getCarrierName() +"\n");
		}
	
	}
	
	//@Test
	public void mathTest() {
		int a = 250/60;
		int b = 250%60;
		
		System.out.println(a + "시간 " + b + "분");
	}
	
	//@Test
	public void unixTime() {
		Calendar c = Calendar.getInstance();
		System.out.println(c.getTimeInMillis()/1000);
		String a = Long.toString(c.getTimeInMillis()/1000);
		System.out.println(a);
	}
	
	//@Test
	public void getPathTest() {
		String curPath = JavaTests.class.getResource("").getPath();
		System.out.println(curPath);
		String sPath = new File("").getAbsolutePath();
		System.out.println(sPath);
	}
	
	@Test
	public void randomNumTest() {
		Random ran = new Random();
		StringBuffer sb = new StringBuffer();
		String strNum = "";
		
		int i =0;
		while (i <50) {
			
			do {
				strNum = String.valueOf(ran.nextInt(10)) ;
				//System.out.println(num);
				sb.append(strNum);
				
			} while (sb.length() < 6);
			System.out.println(sb.toString());
			sb.setLength(0);
			i ++;
		}
		
		 
	}
	
	
}
