
package board;

import retrofit2.Call;
import board.model.response.GroupModel;
import board.utilities.APIInit;

public class CreateMessageGroup {
    public static void main(String[] args) {
        Call<GroupModel> api = APIInit.getAPI().createGroup(APIInit.getHeaders());
        GetMessageGroupInfo.getGroupInfo(api);
    }
}
