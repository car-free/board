package board.board.service;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import board.board.entity.BoardEntity;
import board.board.entity.BoardFileEntity;
import board.board.mapper.BoardMapper;
import board.board.repository.JpaBoardRepository;
import board.common.BoardExceptionType;
import board.common.JpaFileUtils;
import board.common.GrammarException;

@Service
@Transactional(rollbackFor = Exception.class)
public class JpaBoardServiceImpl implements JpaBoardService{
	
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private JpaBoardRepository jpaBoardRepository;
	
	@Autowired
	private JpaFileUtils jpaFileUtils;
	
	@Override
	public List<BoardEntity> selectBoardList() throws Exception {
		return jpaBoardRepository.findAllByOrderByBoardIdxDesc();
	}
	
	@Override
	public void saveBoard(BoardEntity board , MultipartHttpServletRequest multipartRequest) throws Exception {
		if ( board.getContents().contains("sibal")) {
			throw new GrammarException(BoardExceptionType.NOGRAMMAR_INSERT);
		}
		board.setCreatorId("admin");
		List<BoardFileEntity> list = jpaFileUtils.parseFileInfo(multipartRequest);
		if(CollectionUtils.isEmpty(list) ==false) {
			board.setFileList(list);
		}
		jpaBoardRepository.save(board);//insert or update를 둘다 수행
	}

	@Override
	public BoardEntity selectBoardDetail(int boardIdx) throws Exception{
		//jpa에서 find를 	하면 Optional이라는 객체로 받음
		//Optional 객체는 절대로 null이 아님 아래와 같이 받았을때 값이 없더라고 nullpointException을 막을 수 있음
		Optional<BoardEntity> optional = jpaBoardRepository.findById(boardIdx);
		
		if(optional.isPresent()) {
			BoardEntity board = optional.get();
			board.setHitCnt(board.getHitCnt()+1);
			jpaBoardRepository.save(board);
			return board;
		}else {
			throw new NullPointerException();
		}
	}
	

	@Override
	public void deleteBoard(int boardIdx) throws Exception {
		jpaBoardRepository.deleteById(boardIdx);
	}
	
	@Override
	public BoardFileEntity selectBoardFileInfo (int idx, int boardIdx) throws Exception {
		BoardFileEntity boardFile = jpaBoardRepository.findBoardFile(boardIdx, idx);
		return boardFile;
	}
}	

