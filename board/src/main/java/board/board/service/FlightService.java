package board.board.service;

import java.util.ArrayList;
import java.util.List;

import board.board.dto.CodeDto;
import board.board.dto.FlightDto;

public interface FlightService {
	
	List<CodeDto> selectAirportList(String codeId) throws Exception;
	
	ArrayList<FlightDto> selectFlightList(FlightDto flightDto) throws Exception;
	
	void insertReserve(FlightDto flightDto) throws Exception;
	
	ArrayList<FlightDto> selectReserveList() throws Exception;
	
	String retrieveAuth (String mobileNum) throws Exception;
}
