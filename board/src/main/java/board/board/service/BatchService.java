package board.board.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import board.board.dto.AirlinesDto;
import board.board.mapper.FlightMapper;
import board.common.RestAPI;

@Component
public class BatchService {
	
	@Autowired
	private RestAPI restAPI;
	
	@Autowired
	private FlightMapper flightMapper;

	
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	//@Scheduled(cron = "0 2 18 * * *")
	public void updateAirlines() throws Exception{
		log.debug("updateAirlines batch start");
		List<AirlinesDto> airlinesList = restAPI.callAirline();
		if(CollectionUtils.isEmpty(airlinesList)==false) {
			flightMapper.insertAirlinesCode(airlinesList);
		}
	}

}
