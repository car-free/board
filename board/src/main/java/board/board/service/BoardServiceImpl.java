package board.board.service;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import board.board.dto.BoardDto;
import board.board.dto.BoardFileDto;
import board.board.mapper.BoardMapper;
import board.common.BoardExceptionType;
import board.common.FileUtils;
import board.common.GrammarException;

@Service
@Transactional(rollbackFor = Exception.class)
public class BoardServiceImpl implements BoardService{
	
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private BoardMapper boardMapper;
	
	@Autowired
	private FileUtils fileUtils;
	
	@Override
	public List<BoardDto> selectBoardList() throws Exception {
		return boardMapper.selectBoardList();
	}
	
	@Override
	public void insertBoard(BoardDto board , MultipartHttpServletRequest multipartRequest) throws Exception {
		if ( board.getContents().contains("sibal")) {
			throw new GrammarException(BoardExceptionType.NOGRAMMAR_INSERT);
		}
		boardMapper.insertBoard(board);
		//int i = 10 /0;
		Integer boardIdx = boardMapper.selectBoardIdx();
		board.setBoardIdx(boardIdx);
		List<BoardFileDto> fileList =  fileUtils.parseFileInfo(board.getBoardIdx(), multipartRequest);
		if(CollectionUtils.isEmpty(fileList)==false) {
			boardMapper.insertBoardFileList(fileList);
		}
		
		//132 파일테그네임을 가져와서 파일정보를 제대로 보여주는지를 확인하기 위함
		if(!ObjectUtils.isEmpty(multipartRequest)) {
			Iterator<String> fileTagNames = multipartRequest.getFileNames();
			String fileTagName ;
			while(fileTagNames.hasNext()) {
				fileTagName = fileTagNames.next();
				log.debug("file tag name : " + fileTagName);
				List<MultipartFile> list = multipartRequest.getFiles(fileTagName);
				for (MultipartFile file : list) {
					log.debug("start file infomation");
					log.debug("file name : " + file.getOriginalFilename());
					log.debug("file size : " + file.getSize());
					log.debug("file content type  : " + file.getContentType());
					log.debug("end FIle information. \n");
				}
			}
		}
	}

	@Override
	public BoardDto selectBoardDetail(int boardIdx) throws Exception{
		boardMapper.updateHitCount(boardIdx);
		//int i = 10 /0;
		BoardDto board = boardMapper.selectBoardDetail(boardIdx);
		List<BoardFileDto> boardFile = boardMapper.selectBoardFileList(boardIdx);
		board.setFileList(boardFile);
		return board;
	}
	
	@Override
	public void updateBoard(BoardDto board) throws Exception {
		boardMapper.updateBoard(board);
	}

	@Override
	public void deleteBoard(int boardIdx) throws Exception {
		boardMapper.deleteBoard(boardIdx);
	}
	
	@Override
	public BoardFileDto selectBoardFileInfo (int idx, int boardIdx) throws Exception {
		return boardMapper.selectBoardFileInfo(idx, boardIdx);
	}
}	

