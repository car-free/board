package board.board.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import board.board.dto.CodeDto;
import board.board.dto.FlightDto;
import board.board.mapper.FlightMapper;
import board.common.RestAPI;
import board.common.GenNumKey;

@Service
@Transactional(rollbackFor = Exception.class)
public class FlightServiceImpl implements FlightService{
	
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private FlightMapper flightMapper;
	
	@Autowired
	private RestAPI restAPI;
	
	
	@Override
	public List<CodeDto> selectAirportList(String codeId) throws Exception {
		return flightMapper.selectCodeList(codeId);
	}
	
	@Override
	public ArrayList<FlightDto> selectFlightList(FlightDto flightDto) throws Exception {
		ArrayList<FlightDto> flightList = restAPI.callFlights(flightDto);
		return flightList;
	}
	
	@Override
	public void insertReserve(FlightDto flightDto) throws Exception{
		String content = "[" + flightDto.getCarrierName() +"]\n[" + flightDto.getDepartureAirportName() + "]출발  [" + flightDto.getArrivalAirportName() +
				"]도착 \n항공편 예약완료\n출발일 :["+ flightDto.getDepartureTime() +"]" ;
		restAPI.callSms(flightDto.getMobileNum(), content);
		flightMapper.insertReserve(flightDto);
	}
	
	@Override
	public ArrayList<FlightDto> selectReserveList() throws Exception{
		ArrayList<FlightDto> reserveList = flightMapper.selectReserveList();
		return reserveList;
	}

	@Override
	public String retrieveAuth(String mobileNum) throws Exception {
		String authNum = new GenNumKey().getNumKey(6);
		log.debug("authNum -->> " + authNum);
		String content = "인증번호: " + authNum;
		restAPI.callSms(mobileNum, content);
		return authNum;
	}
}	

