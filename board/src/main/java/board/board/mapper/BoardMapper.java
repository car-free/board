package board.board.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import board.board.dto.BoardDto;
import board.board.dto.BoardFileDto;

@Mapper
public interface BoardMapper {
	List<BoardDto> selectBoardList() throws Exception;
	
	void insertBoard(BoardDto board) throws Exception;
	
	void insertBoardFileList(List<BoardFileDto> fileList) throws Exception;

	BoardDto selectBoardDetail(int boardIdx) throws Exception;
	
	List<BoardFileDto> selectBoardFileList (int BoardIdx) throws Exception;
	
	Integer selectBoardIdx () throws Exception;

	void updateHitCount(int boardIdx) throws Exception;
	
	void updateBoard(BoardDto board) throws Exception;
	
	void deleteBoard(int boardIdx) throws Exception;
	
    BoardFileDto selectBoardFileInfo (@Param("idx") int idx, @Param("boardIdx") int boardIdx  ) throws Exception;

}
