package board.board.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import board.board.dto.CodeDto;
import board.board.dto.FlightDto;
import board.board.dto.AirlinesDto;

@Mapper
public interface FlightMapper {
	List<CodeDto> selectCodeList(String codeId) throws Exception;
	CodeDto selectCodeVal (@Param("codeId") String codeId, @Param("code") String code  )   throws Exception;
	void insertAirlinesCode(List<AirlinesDto> airlinesList) throws Exception;
	void insertReserve(FlightDto flightDto) throws Exception;
	ArrayList<FlightDto> selectReserveList () throws Exception;
}
