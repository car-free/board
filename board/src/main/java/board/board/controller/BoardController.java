package board.board.controller;

import java.io.File;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import board.board.dto.BoardDto;
import board.board.dto.BoardFileDto;
import board.board.dto.BoxOfficeDto;
import board.board.service.BoardService;
import board.common.RestAPI;


@Controller
public class BoardController {
	
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private BoardService boardService; 
	
	@Autowired
	private RestAPI restAPI;
	
	@RequestMapping("/board/openBoardList.do")
	public ModelAndView openBoardList() throws Exception{
		
		ModelAndView mv = new ModelAndView("/board/boardList");
		
		List<BoardDto> list = boardService.selectBoardList();
		//int i = 10 /0;
		mv.addObject("list", list);
		
		return mv;
	}
	
	@RequestMapping("/board/openTemp.do")
	public String openTemp() throws Exception{
		return "/board/boardTemp";
	}
	
	@RequestMapping("/board/openBoardWrite.do")
	public String openBoardWrite() throws Exception{
		return "/board/boardWrite";
	}
	
	@RequestMapping("/board/insertBoard.do")
	public String insertBoard(BoardDto board, MultipartHttpServletRequest multipartRequest) throws Exception{
		boardService.insertBoard(board, multipartRequest);
		return "redirect:/board/openBoardList.do";
	}
	
	@RequestMapping("/board/openBoardDetail.do")
	//public ModelAndView openBoardDetail(@RequestParam int boardIdx) throws Exception{ <-- 이렇게 해도 되긴 함
	//public ModelAndView openBoardDetail(int boardId) throws Exception{ <-- 이것도 됨
	public ModelAndView openBoardDetail(@RequestParam("boardIdx") int boardIdx) throws Exception{
		ModelAndView mv = new ModelAndView("/board/boardDetail");
		BoardDto board = boardService.selectBoardDetail(boardIdx);
		
		mv.addObject("board", board);
		
		//ArrayList<HashMap> dailyBoxOfficeResult = restAPI.callAPI();
		//List<HashMap> dailyBoxOfficeResult = restAPI.callAPI();
		List<HashMap> dailyBoxOfficeResult = restAPI.callAPI();
		mv.addObject("dailyBoxOfficeResult", dailyBoxOfficeResult);
		//log.debug("jsonInString ====> " + jsonInString);
		return mv;
	}
	
	@RequestMapping("/board/updateBoard.do")
	public String updateBoard(BoardDto board) throws Exception{
		boardService.updateBoard(board);
		return "redirect:/board/openBoardList.do";
	}
	
	@RequestMapping("/board/deleteBoard.do")
	public String deleteBoard(int boardIdx) throws Exception{
		boardService.deleteBoard(boardIdx);
		return "redirect:/board/openBoardList.do";
	}
	
	@RequestMapping("/board/downloadBoardFile.do")
	public void selectBoardFileInfo(@RequestParam int idx, @RequestParam int boardIdx, HttpServletResponse response) throws Exception{
		BoardFileDto boardFile = boardService.selectBoardFileInfo(idx, boardIdx);
		if(ObjectUtils.isEmpty(boardFile) == false) {
			String fileName = boardFile.getOriginalFileName();
			
			byte[] files = FileUtils.readFileToByteArray(new File(boardFile.getStoredFilePath())); //file을 읽어들인 후 byte배열로 변환
			
			response.setContentType("application/octet-stream");
			response.setContentLength(files.length);
			response.setHeader("Content-Disposition", "attachment; fileName=\"" + URLEncoder.encode(fileName,"UTF-8")+"\";"); //한글로 된 파일이름을 인코딩
			response.setHeader("Content-Transfer-Encoding", "binary");
			
			response.getOutputStream().write(files);
			response.getOutputStream().flush();
			response.getOutputStream().close();
		}
	}
}
