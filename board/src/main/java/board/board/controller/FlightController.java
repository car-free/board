package board.board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import board.board.dto.CodeDto;
import board.board.dto.FlightDto;
import board.board.service.FlightService;
import board.common.RestAPI;


@Controller
public class FlightController {
	
	@Autowired
	private FlightService flightService;
	
	@Autowired
	private RestAPI restAPI;
	
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@RequestMapping("/flight/flightList.do")
	public ModelAndView openFlightList(FlightDto flightDto) throws Exception {
		
		ModelAndView mv = new ModelAndView("/board/flightList");
		
		List<CodeDto> airportList = flightService.selectAirportList("C001");
		
		ArrayList<FlightDto> flightList = new ArrayList<FlightDto>();
		if(ObjectUtils.isEmpty(flightDto.getSrchDepartureAirportFsCode())==false && ObjectUtils.isEmpty(flightDto.getSrchArrivalAirportFsCode())==false
				                                                                    && ObjectUtils.isEmpty(flightDto.getSrchDepartureDay())==false) {//조회조건이 있을때 행공편을 검색
			flightList	= flightService.selectFlightList(flightDto);
		}
		
		mv.addObject("airportList",airportList);
		mv.addObject("flightList",flightList);
		mv.addObject("searchParam",flightDto);
		return mv;
	}
	
	@RequestMapping("/flight/openFlightDetail.do")
	public ModelAndView openFlightDetail(FlightDto flightDto) throws Exception {
		
		ModelAndView mv = new ModelAndView("/board/flightDetail");
		
		
		mv.addObject("flightDetail",flightDto);
		return mv;
	}
	
	@RequestMapping("/flight/insertReserve.do")
	public String insertReserve(FlightDto flightDto) throws Exception {
		flightService.insertReserve(flightDto);
		return "redirect:/flight/openReserveList.do";
	}
	
	@RequestMapping("/flight/openReserveList.do")
	public ModelAndView openReserveList() throws Exception {
		ModelAndView mv = new ModelAndView("/board/reserveList");
		ArrayList<FlightDto> reserveList = flightService.selectReserveList();
		mv.addObject("reserveList",reserveList);
		return mv;
	}
	
	@RequestMapping("/flight/auth")
	public void retrieveAuth(@RequestParam("mobileNum") String mobileNum, HttpServletResponse response) throws Exception {
		log.debug("mobileNum==>>> "+mobileNum);
		try {
			String authNum = flightService.retrieveAuth(mobileNum);
			response.getWriter().print(authNum);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
	
	