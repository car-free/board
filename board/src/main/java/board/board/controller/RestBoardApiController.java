package board.board.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import board.board.dto.BoardDto;
import board.board.service.BoardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.List;

@Api(description = "게시판 REST API")
@RestController
public class RestBoardApiController {
	
	@Autowired
	private BoardService boardService;
	
	@ApiOperation(value = "게시글 목록 조회")
	@RequestMapping(value="/api/board", method=RequestMethod.GET)
	public List<BoardDto> openBoardList() throws Exception{
		
		return boardService.selectBoardList(); //바로 API 의 응답결과로 사용
		
	}
	
	@ApiOperation(value = "게시글 작성")
	@RequestMapping(value="/api/board/write", method=RequestMethod.POST)
	public String insertBoard(@RequestBody  BoardDto board ) throws Exception{
		boardService.insertBoard(board, null);
		return "success";
	}
	
	@ApiOperation(value = "게시글 상세 조회")
	@RequestMapping(value="/api/board/{boardIdx}", method=RequestMethod.GET)
	public BoardDto openBoardDetail(@PathVariable("boardIdx") @ApiParam(value = "게시글 번호", required = true) int boardIdx, ModelMap model) throws Exception{
	//public ModelAndView openBoardDetail(@RequestParam("boardIdx") int boardIdx) throws Exception{ <<--- 안됨 
		//HTTP메소드의 파라미터가 URI의 변수로 사용된다는 의미로 @PathVariable 를 써줌 @RequestParam 으로 하면 안됨
		
		return boardService.selectBoardDetail(boardIdx);
		
	}
	
	@ApiOperation(value = "게시글 수정")
	@RequestMapping(value="/api/board/{boardIdx}", method=RequestMethod.PUT)
	public String updateBoard(@PathVariable("boardIdx")  int boardIdx , @RequestBody BoardDto board) throws Exception{
		board.setBoardIdx(boardIdx);
		boardService.updateBoard(board);
		return "update success";
	}
	
	@ApiOperation(value = "게시글 삭제")
	@RequestMapping(value="/api/board/{boardIdx}", method=RequestMethod.DELETE)
	public String deleteBoard(@PathVariable("boardIdx") @ApiParam(value = "게시글 번호") int boardIdx) throws Exception{
		boardService.deleteBoard(boardIdx);
		return "delete success";
	}
	
}
