package board.board.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import board.board.entity.BoardEntity;
import board.board.entity.BoardFileEntity;

public interface JpaBoardRepository extends CrudRepository<BoardEntity, Integer> {
	 //게시글번호로 정렬해서 전체 게시글을 조회함
	List<BoardEntity> findAllByOrderByBoardIdxDesc();
	                  
	//첨부파일정보조회
	//from 절에 오는 테이블은 테이블이름이 아니라 엔티티를 써줘야 함
	@Query ("SELECT file FROM BoardFileEntity file WHERE board_idx = :boardIdx AND idx = :idx ")
	BoardFileEntity findBoardFile (@Param("boardIdx") int boardIdx, @Param("idx") int idx);
}
