package board.board.dto;

import lombok.Data;

@Data

public class CodeDto {
	
	private String codeId;
	
	private String code;
	
	private String codeNm;
	
	private String extInfo;
	
	private String extraCode;

}
