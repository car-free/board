package board.board.dto;

import lombok.Data;

@Data
public class AirlinesDto {
	
	private String fs;
	
	private String iata;
	
	private String name;
}
