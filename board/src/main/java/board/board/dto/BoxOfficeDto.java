package board.board.dto;

import lombok.Data;

@Data
public class BoxOfficeDto {
	private int rank;
	
	private String movieNm;
	
	private String audiCnt;
	
	private String openDt;
}
