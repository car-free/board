package board.board.dto;

import lombok.Data;

@Data
public class FlightDto {
	
	private String srchDepartureDay;//조회조건용으로 쓰임
	
	private String srchDepartureAirportFsCode; //출발공항코드(조회용)
	
	private String srchArrivalAirportFsCode; //도착공항코드(조회용)
	
	private String departureDay;
	
	private String carrierFsCode;
	
	private String flightNumber;
	
	private String departureAirportFsCode;
	
	private String arrivalAirportFsCode;
	
	private String departureTime;
	
	private String arrivalTime;
	
	private String elapsedTime; //비행시간
	
	//private String depOffsetHours;//출발지 UTC OFFSET 시간 
	
	//private String arrOffsetHours;//도착지 UTC OFFSET 시간
	
	private String carrierName; //항공사명 C002
	
	private String departureAirportName; //출발지 공항명 C001 
	
	private String arrivalAirportName;	//도착지 공항명 C001
	
	private String carrierFlightNumber; //최종편명 carrierFsCode + flightNumber
	
	private String imgLoc ; //항공사 이미지 경로
	
	private String reservIdx; //예약 번호
	
	private String mobileNum ; 
}
