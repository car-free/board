package board.configuration;



import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import board.interceptor.LoggerInterceptor;

@Configuration
//Logger intercepter를 빈으로 등록하기 위함
public class WebMvcConfiguration implements WebMvcConfigurer{
	
	
	@Bean
	public CommonsMultipartResolver multipartResolver() {
		CommonsMultipartResolver commonsmultipartResolver = new CommonsMultipartResolver ();
		commonsmultipartResolver.setDefaultEncoding("UTF-8");
		commonsmultipartResolver.setMaxUploadSize(5*1024*1024);//5mb
		return commonsmultipartResolver;
	}
	
	@Bean
	public HiddenHttpMethodFilter httpMethodFilter (){//http method 에서 put , delete 를 사용하기 위한 트릭용 빈
		HiddenHttpMethodFilter httpMethodFilter = new HiddenHttpMethodFilter();
		return httpMethodFilter;
	}
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new LoggerInterceptor());
	}
}
