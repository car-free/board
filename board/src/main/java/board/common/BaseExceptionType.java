package board.common;

public interface BaseExceptionType {
	int getErrorCode();
	int getHttpStatus();
	String getErrorMessage();
}
