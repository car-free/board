package board.common;

import lombok.Getter;

@SuppressWarnings("serial")
public class GrammarException extends RuntimeException{
	@Getter
    private BaseExceptionType exceptionType;

    public GrammarException(BaseExceptionType exceptionType){
        super(exceptionType.getErrorMessage());
        this.exceptionType = exceptionType;
    }
    
    
}
