package board.common;

import java.io.File;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.MultipartFile;

import board.board.entity.BoardFileEntity;

@Component
public class JpaFileUtils {
	public List<BoardFileEntity> parseFileInfo (MultipartHttpServletRequest multipartRequest) throws Exception{
		if(ObjectUtils.isEmpty(multipartRequest)){
			return null;
		}
		
		List<BoardFileEntity> fileList = new ArrayList<>();
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyyMMdd");
		ZonedDateTime current = ZonedDateTime.now();
		String path = "images/" + current.format(format);
		File file = new File(path);
		if(file.exists()==false) {
			file.mkdirs();
		}
		
		Iterator<String> fileTagNames = multipartRequest.getFileNames();
		String newFileName;
		String originalFileExtension; //유효한 확장자
		String contentType;
		
		while(fileTagNames.hasNext()) {
			List <MultipartFile> files = multipartRequest.getFiles(fileTagNames.next());
			for(MultipartFile multiPartFile : files) {
				if(ObjectUtils.isEmpty(multiPartFile) ==false) {
					contentType = multiPartFile.getContentType();
					if(ObjectUtils.isEmpty(contentType)) {
						break;
					}else {
						if(contentType.contains("jpeg")) {
							originalFileExtension = ".jpg";
						}else if(contentType.contains("png")) {
							originalFileExtension = ".jpg";
						}else if(contentType.contains("gif")) {
							originalFileExtension = ".gif";
						}else break;
					}
					newFileName = Long.toString(System.nanoTime()) + originalFileExtension;
					BoardFileEntity boardFile = new BoardFileEntity();
					boardFile.setFileSize(multiPartFile.getSize());
					boardFile.setOriginalFileName(multiPartFile.getOriginalFilename());
					boardFile.setStoredFilePath(path + "/" + newFileName);
					boardFile.setCreatorId("admin");
					fileList.add(boardFile);
					
					file = new File(path + "/" + newFileName);
					multiPartFile.transferTo(file);
				}
			}
		}
		return fileList;
	}

}
