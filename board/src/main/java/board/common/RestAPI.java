package board.common;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import board.board.dto.AirlinesDto;
import board.board.dto.BoxOfficeDto;
import board.board.dto.FlightDto;
import lombok.extern.slf4j.Slf4j;
import board.board.dto.CommonCode;
import board.common.CommonUtils;
import java.util.Date;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.apache.commons.codec.binary.Hex;
import java.util.UUID;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;
import board.model.request.Message;
import board.model.response.MessageModel;
import board.utilities.APIInit;

@Component
@Slf4j
public class RestAPI {
	@Autowired
	CommonUtils commonUtils;
	
	public ArrayList<HashMap> callAPI() throws JsonProcessingException{
		HashMap<String, Object> result =new HashMap<String, Object>();
		String jsonInString = "";
		ArrayList<HashMap> dailyList = new ArrayList<HashMap>();
		
		try {
			HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
			factory.setConnectTimeout(10000);
			factory.setReadTimeout(10000);
			
			HttpClient httpClient = HttpClientBuilder.create().setMaxConnTotal(100).setMaxConnPerRoute(5).build();
			factory.setHttpClient(httpClient);
			//factory 안쓸려면 factory 부분 날려버리고 그냥 RestTemplate resttemplate = new RestTemplate() 으로 해도 상관없다.
			RestTemplate restTemplate = new RestTemplate(factory);
			
			HttpHeaders header = new HttpHeaders();
			HttpEntity<?> entitiy = new HttpEntity<>(header);
			
			String url = "http://www.kobis.or.kr/kobisopenapi/webservice/rest/boxoffice/searchDailyBoxOfficeList.json";
			UriComponents uri = UriComponentsBuilder.fromHttpUrl(url +"?"+"key=0d727a73d96a952811d12453f639e925&targetDt=20201115").build();
			
			//이 한줄의 코드로 API를 호출해 MAP타입으로 전달 받는다.
			//ResponseEntity<원하는 클래스 타입> resultMap = restTemplate.exchange(uri.toString(), HttpMethod.GET, entity, 원하는클래스타입.class);
			ResponseEntity<Map> resultMap = restTemplate.exchange(uri.toString(), HttpMethod.GET, entitiy, Map.class);
			result.put("statusCode", resultMap.getStatusCodeValue()); //http status code를 확인
			result.put("header", resultMap.getHeaders()); //헤더 정보 확인
			result.put("body", resultMap.getBody()); //실제 데이터 정보 확인
			
			//LinkedHashMap boxOfficeResult = (LinkedHashMap)resultMap.getBody().get("boxOfficeResult");
			HashMap boxOfficeResult = (LinkedHashMap)resultMap.getBody().get("boxOfficeResult");
			log.debug("boxOfficeResult confirm===========  \n"+boxOfficeResult.toString());// hashMap 형태임
			dailyList = (ArrayList<HashMap>)boxOfficeResult.get("dailyBoxOfficeList");
			log.debug("dailyList confirm===========  \n"+dailyList.toString());
			
			
		} catch (HttpClientErrorException | HttpServerErrorException e) {
			result.put("statusCode", e.getRawStatusCode());
			result.put("body", e.getStatusText());
			System.out.println("http error");
			System.out.println(e.toString());
		} catch(Exception e) {
			result.put("statusCode", "999");
			result.put("body", "exception 오류");
			System.out.println(e.toString());
		}
		
		return dailyList;
	}
	
	public ArrayList<AirlinesDto> callAirline() throws JsonProcessingException{
		HashMap<String, Object> result =new HashMap<String, Object>();
		ArrayList<AirlinesDto> airlinesList = new ArrayList<AirlinesDto>();
		
		try {
			HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
			factory.setConnectTimeout(10000);
			factory.setReadTimeout(10000);
			
			HttpClient httpClient = HttpClientBuilder.create().setMaxConnTotal(100).setMaxConnPerRoute(5).build();
			factory.setHttpClient(httpClient);
			//factory 안쓸려면 factory 부분 날려버리고 그냥 RestTemplate resttemplate = new RestTemplate() 으로 해도 상관없다.
			RestTemplate restTemplate = new RestTemplate(factory);
			
			HttpHeaders header = new HttpHeaders();
			HttpEntity<?> entitiy = new HttpEntity<>(header);
			
			String url = CommonCode.AIRLINE_URL;
			UriComponents uri = UriComponentsBuilder.fromHttpUrl(url +"?"+"appId="+CommonCode.APPID+"&appKey="+CommonCode.APPKEY).build();
			
			//이 한줄의 코드로 API를 호출해 MAP타입으로 전달 받는다.
			//ResponseEntity<원하는 클래스 타입> resultMap = restTemplate.exchange(uri.toString(), HttpMethod.GET, entity, 원하는클래스타입.class);
			ResponseEntity<Map> resultMap = restTemplate.exchange(uri.toString(), HttpMethod.GET, entitiy, Map.class);
			result.put("statusCode", resultMap.getStatusCodeValue()); //http status code를 확인
			result.put("header", resultMap.getHeaders()); //헤더 정보 확인
			result.put("body", resultMap.getBody()); //실제 데이터 정보 확인
			
			airlinesList = (ArrayList<AirlinesDto>)resultMap.getBody().get("airlines");
			log.debug("dailyList confirm===========  \n"+airlinesList.toString());
			
			
		} catch (HttpClientErrorException | HttpServerErrorException e) {
			result.put("statusCode", e.getRawStatusCode());
			result.put("body", e.getStatusText());
			System.out.println("http error");
			System.out.println(e.toString());
		} catch(Exception e) {
			result.put("statusCode", "999");
			result.put("body", "exception 오류");
			System.out.println(e.toString());
		}
		
		return airlinesList;
	}
	
	public ArrayList<FlightDto> callFlights(FlightDto flightDto) throws JsonProcessingException{
		HashMap<String, Object> result =new HashMap<String, Object>();
		ArrayList<FlightDto> flightsList = new ArrayList<FlightDto>();
		ArrayList<LinkedHashMap<String,String>> jsonFlightsList = new ArrayList<LinkedHashMap<String,String>>();
		
		try {
			HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
			factory.setConnectTimeout(10000);
			factory.setReadTimeout(10000);
			
			HttpClient httpClient = HttpClientBuilder.create().setMaxConnTotal(100).setMaxConnPerRoute(5).build();
			factory.setHttpClient(httpClient);
			//factory 안쓸려면 factory 부분 날려버리고 그냥 RestTemplate resttemplate = new RestTemplate() 으로 해도 상관없다.
			RestTemplate restTemplate = new RestTemplate(factory);
			
			HttpHeaders header = new HttpHeaders();
			HttpEntity<?> entitiy = new HttpEntity<>(header);
			String urlDepartureDate = flightDto.getSrchDepartureDay();// "yyyy/mm/dd";
			urlDepartureDate = urlDepartureDate.replace("-", "/");
			
			String url = CommonCode.FLIGHTS_URL +"from/"+ flightDto.getSrchDepartureAirportFsCode() + "/to/" + flightDto.getSrchArrivalAirportFsCode() 
			           + "/departing/" + urlDepartureDate + "?extendedOptions=includeNewFields&";
			UriComponents uri = UriComponentsBuilder.fromHttpUrl(url +"appId="+CommonCode.APPID+"&appKey="+CommonCode.APPKEY).build();
			log.debug("final uri ==>>> "+uri.toString());
			
			//이 한줄의 코드로 API를 호출해 MAP타입으로 전달 받는다.
			//ResponseEntity<원하는 클래스 타입> resultMap = restTemplate.exchange(uri.toString(), HttpMethod.GET, entity, 원하는클래스타입.class);
			ResponseEntity<Map> resultMap = restTemplate.exchange(uri.toString(), HttpMethod.GET, entitiy, Map.class);
			
			result.put("statusCode", resultMap.getStatusCodeValue()); //http status code를 확인
			result.put("header", resultMap.getHeaders()); //헤더 정보 확인
			result.put("body", resultMap.getBody()); //실제 데이터 정보 확인
			jsonFlightsList = (ArrayList<LinkedHashMap<String,String>>)resultMap.getBody().get("scheduledFlights");
			
			
			/*
			 * 비행에 걸리는 시간을 계산하기 위해 공항의 offset 시간을 구해서 시차를 구할려고 했는데 api에서 비행시간 정보도 제공해준다는 걸 나중에 알아서 필요가 없어져서 주석처리함...
			 * 
			String depOffsetHours = "";
			String arrOffsetHours = "";
			
			HashMap<String,HashMap> appendix = (HashMap<String,HashMap>)resultMap.getBody().get("appendix");
			List<HashMap<String,String>> airportInfo = (List<HashMap<String,String>>)appendix.get("airports");
			double d = 0;
			int t = 0;
			for (HashMap<String,String> h : airportInfo) {
				if(flightDto.getDepartureAirportFsCode().equals(h.get("fs"))){
					d = Double.valueOf(String.valueOf(h.get("utcOffsetHours")));
					t = Integer.parseInt(String.valueOf(Math.round(d)));
					depOffsetHours = String.valueOf(t); 
				}
				if(flightDto.getArrivalAirportFsCode().equals(h.get("fs"))){
					d = Double.valueOf(String.valueOf(h.get("utcOffsetHours")));
					t = Integer.parseInt(String.valueOf(Math.round(d)));
					arrOffsetHours =  String.valueOf(t);
				}
			}
			log.debug("depOffsetHours  " + depOffsetHours);
			log.debug("arrOffsetHours  " + arrOffsetHours);
			*/ 
			
			/*
			 * 처음엔 아래와 같이 했었는데 이렇게 하면 ArrayList<FlightDto>로 캐스트 했지만 spring 3.X 버그 때문에 ArrayList<LinkedHashMap> 으로 변환되어서 결국 오류난다. 그래서 어쩔수 없이  
			 * 진작에 LinkedHashMap로 받아버린 다음 그걸 풀어서 ArrayList<FlightDto>에 각각 담아버릴수 밖에 없다.
			 * flightsList = (ArrayList<FlightDto>)resultMap.getBody().get("scheduledFlights");
			*/
			String elapsedTime = "";
			int elapsedTotMinute = 0;
			int elapsedDivHour = 0;
			int elapsedDivMinute = 0;
			SimpleDateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
			SimpleDateFormat newDateFormat = new SimpleDateFormat("yyyy년 MM월 dd일 HH시mm분");
			Date departureDate = new Date();
			Date arrivaleDate = new Date();
			String imgLoc = "";
			int t = 0;
			for (int i = 0; i < jsonFlightsList.size(); i++) {
				FlightDto scheduledFlights = new FlightDto();
				scheduledFlights.setCarrierFsCode(jsonFlightsList.get(i).get("carrierFsCode"));
				scheduledFlights.setFlightNumber(jsonFlightsList.get(i).get("flightNumber"));
				scheduledFlights.setDepartureAirportFsCode(jsonFlightsList.get(i).get("departureAirportFsCode"));
				scheduledFlights.setArrivalAirportFsCode(jsonFlightsList.get(i).get("arrivalAirportFsCode"));
				
				departureDate = inputDateFormat.parse(jsonFlightsList.get(i).get("departureTime"));
				arrivaleDate = inputDateFormat.parse(jsonFlightsList.get(i).get("arrivalTime"));
				scheduledFlights.setDepartureTime(newDateFormat.format(departureDate));
				scheduledFlights.setArrivalTime(newDateFormat.format(arrivaleDate));
				
				elapsedTotMinute = Integer.valueOf(String.valueOf(jsonFlightsList.get(i).get("elapsedTime")));
				if(elapsedTotMinute >= 60) {
					elapsedDivHour = elapsedTotMinute / 60 ;
					elapsedDivMinute = elapsedTotMinute % 60 ;
					elapsedTime = elapsedDivHour + "시간 " + elapsedDivMinute + "분";
				}else {
					elapsedTime = elapsedTotMinute + "분";
				}
				scheduledFlights.setElapsedTime(elapsedTime);
				scheduledFlights.setCarrierFlightNumber(commonUtils.getCodeVal("C002", jsonFlightsList.get(i).get("carrierFsCode"), 3)  
						                              + jsonFlightsList.get(i).get("flightNumber"));//IATA 코드 + 항공편넘버링
				scheduledFlights.setCarrierName(commonUtils.getCodeVal("C002" , jsonFlightsList.get(i).get("carrierFsCode"),1));//항공사이름
				scheduledFlights.setDepartureAirportName(commonUtils.getCodeVal("C001" , jsonFlightsList.get(i).get("departureAirportFsCode"),2));///출발공항이름
				scheduledFlights.setArrivalAirportName(commonUtils.getCodeVal("C001" , jsonFlightsList.get(i).get("arrivalAirportFsCode"),2));//도착공항이름
				imgLoc = "/images/carrier/" + jsonFlightsList.get(i).get("carrierFsCode").replace("*", "") + ".png";
				scheduledFlights.setImgLoc(imgLoc);
				
				flightsList.add(i, scheduledFlights);
				
			}
			
				
				//f.setCarrierFlightNumber(f.getCarrierFsCode() + f.getFlightNumber());
			
			/*
			for (FlightDto f : flightsList) {
				log.debug(f.getCarrierFsCode());
				log.debug(f.getFlightNumber());
				log.debug(f.getCarrierFlightNumber()+ "\n");
			}*/
			
			
		} catch (HttpClientErrorException | HttpServerErrorException e) {
			result.put("statusCode", e.getRawStatusCode());
			result.put("body", e.getStatusText());
			System.out.println("http error");
			System.out.println(e.toString());
		} catch(Exception e) {
			result.put("statusCode", "999");
			result.put("body", "exception 오류");
			System.out.println(e.toString());
		}
		
		return flightsList;
	}	
	
	public String callSms(String mobileNum , String content) throws JsonProcessingException{
		Message message = new Message(mobileNum, CommonCode.SMS_FROM_NUM, content);
        Call<MessageModel> api = APIInit.getAPI().sendMessage(APIInit.getHeaders(), message);
        api.enqueue(new Callback<MessageModel>() {
            @Override
            public void onResponse(Call<MessageModel> call, Response<MessageModel> response) {
                // 성공 시 200이 출력됩니다.
                if (response.isSuccessful()) {
                    System.out.println("statusCode : " + response.code());
                    MessageModel body = response.body();
                    System.out.println("groupId : " + body.getGroupId());
                    System.out.println("messageId : " + body.getMessageId());
                    System.out.println("to : " + body.getTo());
                    System.out.println("from : " + body.getFrom());
                    System.out.println("type : " + body.getType());
                    System.out.println("statusCode : " + body.getStatusCode());
                    System.out.println("statusMessage : " + body.getStatusMessage());
                    System.out.println("customFields : " + body.getCustomFields());
                } else {
                    try {
                        System.out.println(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<MessageModel> call, Throwable throwable) {
                throwable.printStackTrace();
            }
        });
    
		
		return "";
	}
	

}
