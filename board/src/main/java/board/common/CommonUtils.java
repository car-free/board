package board.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import board.board.dto.CodeDto;
import board.board.mapper.FlightMapper;

@Component
public class CommonUtils {
	
	@Autowired
	private FlightMapper flightMapper;
	
	public String getCodeVal(String codeId,String code, int option) throws Exception{
		CodeDto codeDto = flightMapper.selectCodeVal(codeId,code);
		String codeVal = "";
		switch (option) {
		case 1: 
			codeVal = codeDto.getCodeNm();
			break;
		case 2: 
			codeVal = codeDto.getExtInfo();
			break;
		case 3:
			codeVal = codeDto.getExtraCode();
			break;		
		default:
			break;
		}
		return codeVal;
	}
}
