
function sample (){
	alert("dd");
};

jQuery(function(){
	//달력 input에서 과거날짜는 비활성화
	$.fn.disableLastDay = function(){
		var d = new Date();
		var toMonth = d.getMonth()+1;
		var toDay = d.getDate();
		
		if (toMonth<10){
			toMonth = "0" + toMonth;
		}
		if (toDay<10){
			toDay = "0" + toDay ;
		}
		var date= d.getFullYear() +"-" + toMonth + "-" +toDay;
		this.attr("min",date);
	};
	
	//class tag가 number것들은 한글입력 받지 않도록 함
	$(".number").keyup(function(event){
        if (!(event.keyCode >=37 && event.keyCode<=40)) {
            var inputVal = $(this).val();
            var check = /[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/;
            if(check.test(inputVal)){
             	$(this).val("");
            }
        }
     });
});